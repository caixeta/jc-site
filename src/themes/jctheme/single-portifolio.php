<?php get_header(); ?>
<div class="content-wrapper">
	<div class="container">
		<?php
			$actual_cat_name = get_queried_object()->name;

 			$current_language = pll_current_language('locale');
 			$language = 'pt-br';
 			$destaqueCat = 'destaques';
 			$notFoundPosts = 'Nenhum portifólio encontrado.';
 			$clienteStr = 'Cliente';
 			$parceiroStr = 'Parceiros';
 			$areaStr = 'Área';
 			$voltarStr = 'Voltar';
 			$pagePortifolio = get_id_by_slug( 'portfolios-pt' );
 			$pagePortifolioURL = get_post_permalink($pagePortifolio); 
 			if($current_language == 'en_US'){
 				$language = 'en-us';
 				$destaqueCat = 'featured';
 				$notFoundPosts = 'No portfolio found.';
 				$clienteStr = 'Customer';
 				$parceiroStr = 'Partners';
 				$areaStr = 'Area';
 				$voltarStr = 'Go back';
 				$pagePortifolio = get_id_by_slug( 'portfolios-en' );
	 			$pagePortifolioURL = get_post_permalink($pagePortifolio); 
 			}
 			if($current_language == 'es_ES'){
 				$language = 'es-es';
 				$destaqueCat = 'destaques-es';
 				$notFoundPosts = 'No se encontró la cartera.';
 				$clienteStr = 'Cliente';
 				$parceiroStr = 'Socios';
 				$areaStr = 'Área';
 				$voltarStr = 'Vuelta atrás';
 				$pagePortifolio = get_id_by_slug( 'portfolios-es' );
	 			$pagePortifolioURL = get_post_permalink($pagePortifolio); 
 			}
		?>
		<section id="portifolio-single">
			<?php
				// The Loop
				if ( have_posts() ) : while ( have_posts() ) : the_post(); 
				$img_id = get_post_thumbnail_id(); 
				$image = wp_get_attachment_image_src($img_id); 
				$alt_text = get_post_meta($img_id , '_wp_attachment_image_alt', true);
				$categories = get_the_terms(get_the_id(), 'portifolios');
				$categoriesNames = '';
				foreach ($categories as $category) {
					if(strcmp($category->slug, $destaqueCat)){
						$categoriesNames[] = $category->name;
					}
				}
				$informacoes_portifolios = get_post_meta( get_the_id(), 'info_portifolio', true );
				if(!empty($informacoes_portifolios)){
					$cliente = $informacoes_portifolios[0]['cliente'];
					$parceiro = $informacoes_portifolios[0]['parceiro'];
				}
				$imgs_portifolios = get_post_meta( get_the_id(), 'imgs_portifolio', true );
			?>
			<div class="row">
				<div class="col-xs-12 portifolios-single">
					<img class="img-responsive img-portifolios-single" src="<?php the_post_thumbnail_url('full');?>" alt="<?php echo $alt_text;?>">
				</div>
			</div>
			<div class="row">
				<div class="portifolios-single-info-wrapper">
					<?php if(isset($cliente) && !is_null($cliente) && !empty($cliente)) { ?>
					<div class="col-md-4 text-center">
						<p class="portifolios-single-titulo-info texto-maiusculo"><?php echo $clienteStr;?></p>
						<p class="portifolios-single-info-text texto-maiusculo"><?php echo $cliente;?></p>
					</div>
					<?php } ?>
					<div class="col-md-4 text-center">
						<p class="portifolios-single-titulo-info texto-maiusculo"><?php echo $areaStr;?></p>
						<?php foreach ($categoriesNames as $category) { ?>
						<p class="portifolios-single-info-text texto-maiusculo"><?php echo $category;?></p>
						<?php }?>
					</div>
					<?php if(isset($parceiro) && !is_null($parceiro) && !empty($parceiro)) { ?>
					<div class="col-md-4 text-center">
						<p class="portifolios-single-titulo-info texto-maiusculo"><?php echo $parceiroStr;?></p>
						<p class="portifolios-single-info-text texto-maiusculo"><?php echo $parceiro;?></p>
					</div>
					<?php } ?>
				</div>
			</div>	
			<div class="row">
				<div class="portifolios-single-content-wrapper">
					<h1 class="texto-maiusculo text-center"><?php the_title();?></h1>
	    			<hr class="separator hidden-xs">
	    			<div class="portifolios-single-content">
						<?php the_content();?>
					</div>
				</div>
				<?php if(isset($imgs_portifolios) && !is_null($imgs_portifolios) && !empty($imgs_portifolios)) { ?>
				<div class="portifolios-single-images">
					<?php foreach ($imgs_portifolios as $img_portifolio) {
						$imagem_src = wp_get_attachment_image_src( $img_portifolio['imagem'], 'full');
		                echo '<div class="col-xs-12 portifolios-single-image">';
		                echo '<img class="img-responsive" src="' . $imagem_src[0] . '"/>';
		                echo '</div>';
					}?>
				</div>
				<?php } ?>
			</div>
			<?php endwhile;
			else:
			?>
				<h2 class="text-center"><?php echo $notFoundPosts; ?>. =(</h2>
			<?php
				endif;
				// Restore original Post Data
				wp_reset_postdata();
			?>
			<div class="row">
			<div class="col-xs-12 col-xs-offset-3 col-sm-4 col-sm-offset-4 col-lg-2 col-lg-offset-5">
				<div class="col-xs-12 cta-container cta-margin-top">
					<a href="<?php echo $pagePortifolioURL;?>" class="btn-action btn-principal texto-maiusculo"><?php echo $voltarStr;?></a>
				</div>
			</div>
			</div>
		</section>			
		<div class="menu-footer">
			<?php    /**
	        * Displays a navigation menu
	        * @param array $args Arguments
	        */
	        $args = array(
	            'theme_location' => 'footer-menu',
	            'items_wrap' => '<ul class="nav navmenu-nav nav-justified clearfix">%3$s</ul>',
	        );

	        wp_nav_menu( $args );
	    ?>
	    <div class="text-center">
	    	<?php include('components/social_list.php');?>
	    </div>
	    </div>
	</div>
</div>

<?php get_footer(); ?>