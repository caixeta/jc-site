<?php get_header(); ?>
<div class="content-wrapper">
	<div class="container">
		<?php
			$actual_cat_name = get_queried_object()->name;
			$actual_cat_slug = get_queried_object()->slug;

 			$current_language = pll_current_language('locale');
 			$language = 'pt-br';
 			$destaqueCat = 'destaques';
 			$notFoundPosts = 'Nenhum portifólio encontrado.';
 			if($current_language == 'en_US'){
 				$language = 'en-us';
 				$destaqueCat = 'featured';
 				$notFoundPosts = 'No portfolio found.';
 			}
 			if($current_language == 'es_ES'){
 				$language = 'es-es';
 				$destaqueCat = 'destaques-es';
 				$notFoundPosts = 'No se encontró la cartera.';
 			}
		?>
    	<?php include('components/categorias_list.php');?>

    	<hr class="separator hidden-xs">

		<section id="portifolios-thumbs">
			<div class="row">
			<?php
				// WP_Query arguments
				$args = array (
					'post_type'    	=> 'portifolio',
					'portifolios'	=> $actual_cat_slug,
					'language'		=> $language
				);

				// The Query
				$query = new WP_Query( $args );
				// The Loop
				if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); 
				$img_id = get_post_thumbnail_id(); 
				$image = wp_get_attachment_image_src($img_id); 
				$alt_text = get_post_meta($img_id , '_wp_attachment_image_alt', true);
				$categories = get_the_terms(get_the_id(), 'portifolios');
				$categoriesNames = '';
				foreach ($categories as $category) {
					if(strcmp($category->slug, $destaqueCat)){
						$categoriesNames[] = $category->name;
					}
				}
			?>
				<div class="col-xs-12 col-sm-6 portifolios-dados-container">
					<a href="<?php echo get_post_permalink();?>" title="<?php echo get_the_title();?>" class="portifolios-link">
						<img class="img-responsive img-bg-portifolios" src="<?php the_post_thumbnail_url('full');?>" alt="<?php echo $alt_text;?>">
						<div class="portifolios-dados-wrapper">
							<h5 class="texto-maiusculo portifolios-title"><b><?php echo get_the_title(); ?></b></h5>
							<ul class="portifolios-list-name">
								<?php foreach ($categoriesNames as $category) { ?>
								<li class="texto-maiusculo portifolios-list-cat-name"><?php echo $category;?></li>
								<?php }?>
							</ul>
						</div>
					</a>
				</div>	
			<?php endwhile;
			else:
			?>
				<h2 class="text-center"><?php echo $notFoundPosts; ?>. =(</h2>
			<?php
				endif;
				// Restore original Post Data
				wp_reset_postdata();
			?>
			</div>
		</section>		
		<?php include('components/contato_footer.php');?>
	</div>
</div>

<?php get_footer(); ?>