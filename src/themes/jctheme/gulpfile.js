/* Constantes */
const gulp = require('gulp');
const sass = require('gulp-ruby-sass');
const autoprefixer = require('gulp-autoprefixer');
const concat = require('gulp-concat');
const jshint = require('gulp-jshint');
const uglify = require('gulp-uglify');

/* Tasks */
/* Compilar Sass em CSS */
gulp.task('build-css', function() {
    return sass('develop/scss/**/*.scss')
		    .on('error', sass.logError)
		    .pipe(autoprefixer())
			.pipe(concat('style.css'))
		    .pipe(gulp.dest('assets/css'));
});

/* Verificação do JavaScript*/
gulp.task('lint', function() {
	return gulp.src('develop/js/*.js')
		  	.pipe(jshint())
			.pipe(jshint.reporter('jshint-stylish'))
		  	.pipe(jshint.reporter('fail'));

});

/* Comprimir JavaScript */
gulp.task('compress', function() {
  	return gulp.src('develop/js/**/*.js')
	    	.pipe(uglify())
			.pipe(concat('scripts.js'))
	    	.pipe(gulp.dest('assets/js'));
});

/* Task de Watch*/
gulp.task('watch', function() {
  gulp.watch('develop/scss/**/*.scss', ['build-css']);
  gulp.watch('develop/js/*.js', ['lint','compress']);
});

/* Definição da função Default*/
gulp.task('default', ['watch']);