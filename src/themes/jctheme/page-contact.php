<?php get_header(); ?>
<h1 class="hidden"><?php wp_title();?></h1>
<div class="content-wrapper">
	<div class="container">
		<?php 
			if ( have_posts() ) : while ( have_posts() ) : the_post();
		?>
		<section id="contato-section" class="clearfix">
			<div class="row">	
				<img src="<?php echo get_template_directory_uri();?>/assets/img/RAMO_2.png" class="img-responsive img-contato" alt="Contato"/>
			</div>
			<div class="row row-titulo-contato">	
				<h2 class="texto-maiusculo text-center"><?php echo get_the_title();?></h2>
				<div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 text-center">
					<p><a href="mailto:<?php echo get_informacao('endemail')?>?subject=Contato Site" class="big-text texto-maiusculo">
						<?php echo get_informacao('endemail')?>
					</a></p>
					<p><b class=" texto-cor-secundaria"><?php echo get_informacao('pais_ddd')?></b> <b class="medium-text"><?php echo get_informacao('telefone')?></b></p>
					<?php include('components/social_list.php');?>
				</div>
			</div>
		</section>
		<section id="form-contato-section">
			<div class="row">
				<!-- <form action="" method="POST" role="form" class="col-xs-12 col-md-offset-2 col-md-8 text-center"> -->
					<!-- <legend>Entre em contato pra falar sobre um projeto ou esclarecer qualquer duvida:</legend> -->
					<?php the_content();?>
				<!-- </form> -->
			</div>			
			<img src="<?php echo get_template_directory_uri();?>/assets/img/FLOR.png" alt="Contato" class="img-responsive img-end-contato"/>
		</section>
	<?php endwhile; endif;?>
	</div>
</div>

<?php get_footer(); ?>