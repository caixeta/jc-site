<?php get_header(); ?>
<div class="content-wrapper">
	<div class="container">
		<section id="portifolios-thumbs">
			<div class="row">
			<?php
				// WP_Query arguments
				$args = array (
					'post_type'    	=> 'portifolio',
					'language' 		=> 'es-es'
				);

				// The Query
				$query = new WP_Query( $args );
				// The Loop
				if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); 
				$img_id = get_post_thumbnail_id(); 
				$image = wp_get_attachment_image_src($img_id); 
				$alt_text = get_post_meta($img_id , '_wp_attachment_image_alt', true);
				$categories = get_the_terms(get_the_id(), 'portifolios');
				$categoriesNames = '';
				foreach ($categories as $category) {
					if(strcmp($category->slug, 'destaques-es')){
						$categoriesNames[] = $category->name;
					}
				}
			?>
				<div class="col-xs-12 col-sm-6 portifolios-dados-container">
					<a href="<?php echo get_post_permalink();?>" title="<?php echo get_the_title();?>" class="portifolios-link">
						<img class="img-responsive img-bg-portifolios" src="<?php the_post_thumbnail_url('full');?>" alt="<?php echo $alt_text;?>">
						<div class="portifolios-dados-wrapper">
							<h5 class="texto-maiusculo portifolios-title"><b><?php echo get_the_title(); ?></b></h5>
							<ul class="portifolios-list-name">
								<?php foreach ($categoriesNames as $category) { ?>
								<li class="texto-maiusculo portifolios-list-cat-name"><?php echo $category;?></li>
								<?php }?>
							</ul>
						</div>
					</a>
				</div>
			<?php endwhile;
			else:
			?>
				<h2 class="text-center">No se encontró la cartera. =(</h2>
			<?php
				endif;
				// Restore original Post Data
				wp_reset_postdata();
			?>
			</div>
		</section>		
		<div class="menu-footer">
			<?php    /**
	        * Displays a navigation menu
	        * @param array $args Arguments
	        */
	        $args = array(
	            'theme_location' => 'footer-menu',
	            'items_wrap' => '<ul class="nav navmenu-nav nav-justified clearfix">%3$s</ul>',
	        );

	        wp_nav_menu( $args );
	    ?>
	    <div class="text-center">
	    	<?php include('components/social_list.php');?>
	    </div>
	    </div>
	</div>
</div>

<?php get_footer(); ?>