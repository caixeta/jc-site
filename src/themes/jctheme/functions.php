<?php
// Menus de navegação

	register_nav_menus(array('off-canvas' => 'Menu Principal (Off Canvas)','footer-menu' => 'Menu Footer'));

// Adicionar suporte à imagem destacada e a resumo nas páginas	

 	add_theme_support('post-thumbnails');
 	add_post_type_support( 'page', 'excerpt' );

// Fila de scripts e styles

function theme_codes_scripts_styles() {
	// Styles
	wp_enqueue_style( 'bootstrap-style', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css', array(), '3.3.6');
    wp_enqueue_style( 'style_jc_theme', get_template_directory_uri() . '/assets/css/style.css', array(), '1.0.0');

    // Scripts
    wp_enqueue_script( 'jquery-script', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js', array(), '2.2.4', true );
    wp_enqueue_script( 'bootstrap-script', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js', array('jquery-script'), '3.3.6', true );
    wp_enqueue_script( 'script_jc_theme', get_template_directory_uri() . '/assets/js/scripts.js', array('bootstrap-script'), '1.0.0', true );

    
}
add_action( 'wp_enqueue_scripts', 'theme_codes_scripts_styles' );

// Adicionar o tipo de post de portifólio

	add_action('init', 'portifolio_post_type_init');

	function portifolio_post_type_init() { 
		$labels = array(
			'name' => _x('Portifólio', 'post type general name'),
			'singular_name' => _x('Trabalho de Portifólio', 'post type singular name'),
			'add_new' => _x('Adicionar Novo', 'Novo item'),
			'add_new_item' => __('Novo Trabalho de Portifólio'),
			'edit_item' => __('Editar Trabalho de Portifólio'),
			'new_item' => __('Novo Trabalho de Portifólio'),
			'view_item' => __('Ver Trabalho de Portifólio'),
			'search_items' => __('Procurar Trabalhos de Portifólio'),
			'not_found' =>  __('Nenhum Trabalho de Portifólio encontrado'),
			'not_found_in_trash' => __('Nenhum Trabalho de Portifólio encontrado na lixeira'),
			'parent_item_colon' => '',
			'menu_name' => 'Trabalhos de Portifólio'
		);

		$args = array(
			'labels' => $labels,
			'public' => true,
			'public_queryable' => true,
			'show_ui' => true,			
			'query_var' => true,
			'rewrite' => true,
			'capability_type' => 'post',
			'has_archive' => true,
			'hierarchical' => false,
			'menu_position' => null,
			'menu_icon' => 'dashicons-portfolio',	
			'supports' => array('title','editor','thumbnail', 'excerpt', 'custom-fields', 'revisions', 'trackbacks')
		  ); 
		register_post_type( 'portifolio' , $args );
		flush_rewrite_rules();
	}

	register_taxonomy(
		"portifolios", 
		"portifolio", 
		array(            
			"label" => "Categorias de Trabalho de Portifólio", 
		    "singular_label" => "Categoria de Trabalho de Portifólio", 
		    "rewrite" => true,
		    "hierarchical" => true
		)
	);

// Adicionar menu de redes sociais

add_action ( "admin_menu", "criar_menu_informacoes" );
add_action ( 'admin_init', 'criar_setting_informacoes' );

function criar_menu_informacoes () {
	add_submenu_page ( 'themes.php', 'Informações', 'Informações', 'manage_options', 'informacao', 'criar_pagina_informacoes' );
}

function criar_setting_informacoes() {
	register_setting ( 'informacoes', 'informacoes' );
	add_settings_section ( 'informacoes', 'Informações', 'section_informacao', 'theme_informacoes' );
}
	
function section_informacao() {
	
	add_settings_field ( 'linkedin', 'LinkedIn', 'linkedin_field', 'theme_informacoes', 'informacoes' );
	add_settings_field ( 'instagram', 'Instagram', 'instagram_field', 'theme_informacoes', 'informacoes' );
	add_settings_field ( 'behance', 'Behance', 'behance_field', 'theme_informacoes', 'informacoes' );
	add_settings_field ( 'dribble', 'Dribble', 'dribble_field', 'theme_informacoes', 'informacoes' );
	add_settings_field ( 'pinterest', 'Pinterest', 'pinterest_field', 'theme_informacoes', 'informacoes' );
	
	add_settings_field ( 'pais_ddd', 'Código do páis e DDD', 'pais_ddd_field', 'theme_informacoes', 'informacoes' );
	add_settings_field ( 'telefone', 'Telefone', 'telefone_field', 'theme_informacoes', 'informacoes' );
	add_settings_field ( 'endemail', 'Email', 'endemail_field', 'theme_informacoes', 'informacoes' );

	add_settings_field ( 'txtBR', 'Texto Index BR', 'txtBR_field', 'theme_informacoes', 'informacoes' );
	add_settings_field ( 'txtEN', 'Texto Index EN', 'txtEN_field', 'theme_informacoes', 'informacoes' );
	add_settings_field ( 'txtES', 'Texto Index ES', 'txtES_field', 'theme_informacoes', 'informacoes' );

}

function criar_pagina_informacoes() {
	echo '<div id="theme-options-wrap">';
	echo '<form method="post" action="options.php" enctype="multipart/form-data">';
	settings_fields('informacoes');
	do_settings_sections('theme_informacoes');
	echo '<p class="submit">';
	submit_button();
	echo '</p>';
	echo '</form>';
	echo '</div>';
}

function linkedin_field() {
	$options = get_option ( 'informacoes' );
	echo '<input name="informacoes[linkedin]" type="text" value="' . $options["linkedin"] . '" />';
}
function instagram_field() {
	$options = get_option ( 'informacoes' );
	echo '<input name="informacoes[instagram]" type="text" value="' . $options["instagram"] . '" />';
}
function behance_field() {
	$options = get_option ( 'informacoes' );
	echo '<input name="informacoes[behance]" type="text" value="' . $options["behance"] . '" />';
}
function dribble_field() {
	$options = get_option ( 'informacoes' );
	echo '<input name="informacoes[dribble]" type="text" value="' . $options["dribble"] . '" />';
}
function pinterest_field() {
	$options = get_option ( 'informacoes' );
	echo '<input name="informacoes[pinterest]" type="text" value="' . $options["pinterest"] . '" />';
}

function pais_ddd_field() {
	$options = get_option ( 'informacoes' );
	echo '<input name="informacoes[pais_ddd]" type="text" value="' . $options["pais_ddd"] . '" />';
}

function telefone_field() {
	$options = get_option ( 'informacoes' );
	echo '<input name="informacoes[telefone]" type="text" value="' . $options["telefone"] . '" />';
}
function endemail_field() {
	$options = get_option ( 'informacoes' );
	echo '<input name="informacoes[endemail]" type="text" value="' . $options["endemail"] . '" />';
}
function txtBR_field() {
	$options = get_option ( 'informacoes' );
	echo '<textarea name="informacoes[txtBR]">' . $options["txtBR"] . '  </textarea>';
}
function txtEN_field() {
	$options = get_option ( 'informacoes' );

	echo '<textarea  name="informacoes[txtEN]">' . $options["txtEN"] . ' </textarea>';
}
function txtES_field() {
	$options = get_option ( 'informacoes' );
	echo '<textarea name="informacoes[txtES]">' . $options["txtES"] . '  </textarea>';
}

function get_informacao($rs) {
	$options = get_option ( 'informacoes' );
	return $options [$rs];
}

//Contact Form 7
add_filter( 'wpcf7_form_class_attr', 'custom_form_class_attr' );

function custom_form_class_attr( $class ) {
	$class .= ' col-xs-12 col-md-offset-2 col-md-8 text-center';
	return $class;
}

function get_id_by_slug($page_slug) {
	$page = get_page_by_path($page_slug);
	if ($page) {
		return $page->ID;
	} else {
		return null;
	}
}

?>