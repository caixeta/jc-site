<?php get_header(); ?>
<h1 class="hidden"><?php wp_title();?></h1>
<div class="content-wrapper">
	<div class="container">
		<?php 
			if ( have_posts() ) : while ( have_posts() ) : the_post();
				$citacao = get_post_meta( get_the_id(), 'citacao', true );
				$educacao = get_post_meta( get_the_id(), 'educacao', true );
				$experiencia = get_post_meta( get_the_id(), 'experiencia', true );
				$idiomas = get_post_meta( get_the_id(), 'idiomas', true );
				$habilidades = get_post_meta( get_the_id(), 'habilidades', true );
				$img_id = get_post_thumbnail_id(); 
				$image = wp_get_attachment_image_src($img_id); 
				$alt_text = get_post_meta($img_id , '_wp_attachment_image_alt', true);
		?>
		<section id="sobre-section" class="clearfix">
			<div class="row">	
				<div class="col-xs-12">
					<p class="text-center texto-sobre">
						<?php echo the_content();?>
					</p>
					<div class="col-xs-12 col-md-offset-2 col-md-8 perfil">
						<img src="<?php the_post_thumbnail_url('full');?>" alt="<?php echo $alt_text;?>" class="img-responsive">	
						<blockquote class="col-md-5 pull-right col-sm-6">
							<p class="texto-maiusculo text-right"><?php echo $citacao[0]['citacao']?></p>
						</blockquote>
					</div>
				</div>
			</div>
		</section>
		<section id="curriculo-section">
			<div class="row">
				<h2 class="col-xs-6 col-sm-4 col-md-3 texto-maiusculo titulo-section row-sm-down">Educación</h2>
				<div class="col-xs-12 col-sm-8">
					<?php foreach ($educacao as $edu) { ?>
						<p class="subtitulo conteudo-subtitulo texto-maiusculo"><b><?php echo $edu['titulo'];?></b></p>
						<p class="texto-cor-secundaria texto-maiusculo"><?php echo $edu['instituicao'];?></p>
						<p class="texto-cor-secundaria texto-maiusculo"><?php echo $edu['ano'];?></p>						
					<?php } ?>
				</div>	
			</div>
			<div class="row">
				<h2 class="col-xs-6 col-sm-4  col-md-3 texto-maiusculo titulo-section row-sm-down">Experiencia</h2>
				<div class="col-xs-12 col-sm-8">
					<?php foreach ($experiencia as $exp) { ?>
						<p class="subtitulo"><b class="conteudo-subtitulo texto-maiusculo"><?php echo $exp['empresa'];?></b><span class="texto-cor-secundaria"><?php echo $exp['periodo'];?></span></p>
						<p class="texto-cor-secundaria texto-maiusculo">
							<?php echo $exp['descricao'];?>
						</p>				
					<?php } ?>
				</div>	
			</div>
			<div class="row">
				<h2 class="col-xs-6 col-sm-4  col-md-3 texto-maiusculo titulo-section row-sm-down">Idiomas</h2>
				<div class="col-xs-12 col-sm-8">
					<?php foreach ($idiomas as $idi) { ?>
						<p><b class="conteudo-subtitulo texto-maiusculo"><?php echo $idi['idioma'];?></b><span class="texto-cor-secundaria"><?php echo $idi['proeficiencia'];?></span></p>			
					<?php } ?>
				</div>	
			</div>
			<div class="row">
				<h2 class="col-xs-6 col-sm-4  col-md-3 texto-maiusculo titulo-section row-sm-down">Habilidades</h2>
				<div class="col-xs-12 col-sm-8">
					<p class="conteudo-subtitulo texto-maiusculo"><b><?php echo $habilidades[0]['habilidades']?></b></p>
				</div>	
			</div>
			
		</section>
		<?php endwhile; endif; ?>
		<?php include('components/contato_footer.php');?>
	</div>
</div>

<?php get_footer(); ?>