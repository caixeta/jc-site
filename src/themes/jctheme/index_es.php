		<?php
		 	$pageSobre = get_page_by_path( 'sobre-es' );
 			$page_excerpt = $pageSobre->post_excerpt;
 			$pageSobreURL = $pageSobre->guid;
 			$pageContato = get_page_by_title( 'Contacto' );
 			$pageContatoURL = $pageContato->guid;
 			$pagePortifolio = get_id_by_slug( 'portfolios-es' );
 			$pagePortifolioURL = get_post_permalink($pagePortifolio);

		?>
		<section id="sobre-section" class="clearfix">
			<div class="left-content">	
				<div class="row">			
					<h2 class="col-xs-6 col-sm-3 col-md-2 texto-maiusculo titulo-section row-sm-down">Sobre</h2>
					<p class="col-xs-12 col-sm-9 col-md-6 paragrafo-descricao">
						<?php echo $page_excerpt;?>
					</p>
				</div>
				<div class="row">
					<div class="col-xs-6 col-md-offset-2 col-sm-3 col-md-2 cta-container">
						<a href="<?php echo $pageSobreURL;?>" class="btn-action btn-principal texto-maiusculo">Sobre</a>
					</div>		
					<div class="col-xs-6 col-sm-3 col-md-2 cta-container">
						<a href="<?php echo $pageContatoURL;?>" class="btn-action btn-principal texto-maiusculo">Contacto</a>
					</div>		
				</div>
			</div>		
			<div class="right-content">	
				<div class="row">			
					<div class="hidden-xs hidden-sm col-md-5 col-lg-5 img-deslocada">
						<img src="<?php echo get_template_directory_uri();?>/assets/img/RAMO.png" class="img-responsive">
					</div>
					<h2 class="col-xs-6 col-sm-offset-3 col-md-offset-inverse-1 col-lg-offset-inverse-1 col-sm-3 col-md-2 texto-maiusculo titulo-section row-sm-down"></h2>		
					<p class="col-xs-12 col-sm-offset-3 col-md-offset-0 col-sm-9 col-md-6 col-lg-6 paragrafo-descricao-secundario texto-cor-secundaria">
						<?php echo get_informacao('txtES')?>
					</p>	
					<div class="col-xs-6 col-sm-offset-3 col-md-offset-1 col-sm-3 col-md-2 cta-container cta-margin-top">
						<a href="<?php echo $pagePortifolioURL;?>" class="btn-action btn-principal texto-maiusculo">Cartera</a>
					</div>	
				</div>
			</div>			
		</section>	

		<section id="home-destaque">
			<?php
				// WP_Query arguments
				$cat = get_term_by( 'name', 'destaques', 'portifolios');
				if ( $cat ){
					$cat_Destaques = $cat->term_id;
					$args = array (
						'post_type'     => 'portifolio',
						'portifolios'	=> 'destaques',
						'language' 		=> 'es-es'
					);

				// The Query
				$query = new WP_Query( $args );
				$count = 1;
				// The Loop
				if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); 
				$img_id = get_post_thumbnail_id(); 
				$image = wp_get_attachment_image_src($img_id); 
				$alt_text = get_post_meta($img_id , '_wp_attachment_image_alt', true);
				$categories = get_the_terms(get_the_id(), 'portifolios');
				$categoryName = '';
				$category_link = get_post_permalink();

				foreach ($categories as $category) {
					if(strcmp($category->slug, 'destaques-es')){
						$categoryName = $category->name;
						//$category_link = get_term_link( $category->slug, 'portifolios');
						break;
					}
				}
				// $category_link = get_category_link( $cat_ID );
			?>
			<div class="row">
				<?php if ( $count % 2 != 0 ) { ?>
				<h2 class="col-xs-6 col-sm-5 col-md-3 texto-maiusculo titulo-section titulo-section-destaque send-front"><?php echo $categoryName;?></h2>
				<div class="col-xs-12 col-md-5 col-md-offset-inverse-1 no-padding">
					<a href="<?php echo $category_link;?>" title="" class="destaque-home-link">
						<img class="img-responsive" src="<?php the_post_thumbnail_url('full');?>" alt="<?php echo $alt_text;?>">
					</a>
				</div>	
				<?php } else { ?>
				<h2 class="col-xs-6 col-sm-5 col-md-3 texto-maiusculo titulo-section titulo-section-destaque send-front text-right pull-right come-back-1"><?php echo $categoryName;?></h2>
				<div class="col-xs-12 col-md-offset-4 col-md-5 no-padding">
					<a href="<?php echo $category_link;?>" title="" class="destaque-home-link">
						<img class="img-responsive" src="<?php the_post_thumbnail_url('full');?>" alt="<?php echo $alt_text;?>">
					</a>
				</div>		
				<?php } $count++;?>	
			</div>
			<?php endwhile; endif;
				// Restore original Post Data
				wp_reset_postdata();
			}
			?>
			<div class="col-xs-6 col-xs-offset-3 col-sm-4 col-sm-offset-4 col-lg-2 col-lg-offset-5">
				<div class="col-xs-12 cta-container cta-margin-top">
					<a href="<?php echo $pagePortifolioURL;?>" class="btn-action btn-principal texto-maiusculo">Ver Cartera</a>
				</div>
			</div>
		</section>
		<?php include('components/contato_footer.php');?>