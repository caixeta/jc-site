function ajustFrame () {
	var newHeight 	= 0;
	var newWidth  	= 0;
	var newSpacing  = 13;
	var borderWidth = 2;

	newHeight = $(window).outerHeight() - (2 * newSpacing);
	newWidth = $(window).width() - (2 * newSpacing);
	$('.site-wrapper').css('height', newHeight + 'px');
	$('.site-wrapper').css('width', newWidth + 'px');
	$('.site-wrapper').css('margin', newSpacing + 'px');
	$('.site-wrapper').css('border-width', borderWidth + 'px');

	newWidth  = $('.site-wrapper').innerWidth();
	$('.logo-wrapper').css('width', newWidth);

	newWidth  = newWidth - (2 * newSpacing);
	newHeight = newHeight - $('.logo-wrapper').outerHeight() - (4 * newSpacing) + borderWidth;
	newSpacing = (newSpacing + borderWidth + $('.logo-wrapper').outerHeight() + 'px') + (' auto ') + (newSpacing + borderWidth + 'px');
	$('.content-wrapper').css('height',newHeight + 'px');
	$('.content-wrapper').css('width',newWidth + 'px');
	$('.content-wrapper').css('margin',newSpacing);
}

$(document).ready(function(){
	$('img.svg').each(function(){
	    var $img = $(this);
	    var imgID = $img.attr('id');
	    var imgClass = $img.attr('class');
	    var imgURL = $img.attr('src');

	    $.get(imgURL, function(data) {
	        // Get the SVG tag, ignore the rest
	        var $svg = $(data).find('svg');

	        // Add replaced image's ID to the new SVG
	        if(typeof imgID !== 'undefined') {
	            $svg = $svg.attr('id', imgID);
	        }
	        // Add replaced image's classes to the new SVG
	        if(typeof imgClass !== 'undefined') {
	            $svg = $svg.attr('class', imgClass+' replaced-svg');
	        }

	        // Remove any invalid XML tags as per http://validator.w3.org
	        $svg = $svg.removeAttr('xmlns:a');

	        // Check if the viewport is set, if the viewport is not set the SVG wont't scale.
	        if(!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
	            $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'));
	        }

	        // Replace image with new SVG
	        $img.replaceWith($svg);

	    }, 'xml');

	});
	ajustFrame();
});

$(window).resize(function () {
	ajustFrame();
});