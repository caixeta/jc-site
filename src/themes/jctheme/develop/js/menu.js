$(document).mouseup(function (e){
    var offcanvas = $('#offcanvas');
    var navIcon = $('#nav-icon');
    if (!offcanvas.is(e.target) && offcanvas.has(e.target).length === 0){
		if(navIcon.is(e.target) || navIcon.has(e.target).length > 0){
			navIcon.toggleClass('open');
			offcanvas.toggleClass('open');
		}
		else{ 
			if(navIcon.hasClass('open')){
				navIcon.toggleClass('open');
			}   
			if(offcanvas.hasClass('open')){
				offcanvas.toggleClass('open');
			}  
		}

    }
    
});

$(document).ready(function(){
    var offcanvas = $('#offcanvas');
    offcanvas.css('min-height',$(window).height());	
    var leftOffCanvas= "-" + offcanvas.outerWidth() + "px";
    offcanvas.css('left', leftOffCanvas);	
});
$(window).on('resize', function () {
    var offcanvas = $('#offcanvas');
    offcanvas.css('min-height',$(window).height());	
    var leftOffCanvas= "-" + offcanvas.outerWidth() + "px";
    offcanvas.css('left', leftOffCanvas);	
});