<!DOCTYPE html>
<html  <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php wp_title();?></title>
	<meta name="description" content="<?php bloginfo( 'description' );?>">
	<link rel="author" href="humans.txt" />
    <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri();?>/assets/img/favicon.png">
    <?php wp_head(); ?>
</head>
<body>
<!-- Header -->

<!-- Off-Canvas Menu -->
<div class="nav-wrapper">
    <div id="nav-icon" class="nav-toggler">
      <span></span>
      <span></span>
      <span></span>
    </div>
</div>
<nav id="offcanvas" class="navmenu navmenu-inverse navmenu-fixed-right offcanvas navmenu-site" role="navigation">   
      
    <?php    /**
        * Displays a navigation menu
        * @param array $args Arguments
        */
        $args = array(
            'theme_location' => 'off-canvas',
            'items_wrap' => '<ul class = "nav navmenu-nav clearfix">%3$s</ul>',
        );

        wp_nav_menu( $args );
    ?>
    <!--<ul class="nav navmenu-nav clearfix"> 
        <li><a href="sobre.php">Sobre</a></li>
        <li><a href="contato.php">Contato</a></li>
        <li class="dropdown">
            <a href="#">Portifólio <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="#">Todos</a></li>
                <li><a href="#">Id. Visual</a></li>
                <li><a href="#">Embalagem</a></li>
                <li><a href="#">Web/App</a></li>
                <li><a href="#">Impresso</a></li>
                <li><a href="#">Ilustração</a></li>
                <li><a href="#">Caligrafia</a></li>
            </ul>
        </li>
    </ul> -->
    

    <ul class="list-inline list-i18n">
        <?php $current_language = pll_current_language('locale');?>

        <li><a href="http://joaocezar.com/" hreflang="pt-BR" class="btn-i18n <?php if($current_language == 'pt_BR') echo 'active-i18n'?>">PT</a></li><span class="middot"> &middot; </span>
        <li><a href="<?php echo esc_url( home_url( '/en/' ) );?>" hreflang="en-US" class="btn-i18n <?php if($current_language == 'en_US') echo 'active-i18n'?>">EN</a></li><span class="middot"> &middot; </span>
        <li><a href="<?php echo esc_url( home_url( '/es/' ) );?>" hreflang="es-ES" class="btn-i18n <?php if($current_language == 'es_ES') echo 'active-i18n'?>">ES</a></li>
    </ul>
    <?php include('components/social_list.php');?>
</nav>
<!-- End Off-Canvas Menu -->
<!-- End Header -->

<div class="site-wrapper">
    
</div>