<ul class="list-inline list-social">
    <?php if((get_informacao('linkedin'))){?>
    <li class="social-item">
        <a href="<?php echo get_informacao('linkedin')?>" class="social-link" target="_blank">
            <img src="<?php echo get_template_directory_uri();?>/assets/img/linkedin.svg" class="svg social-svg">
        </a>
    </li>
    <?php }?>
    <?php if((get_informacao('instagram'))){?>
    <li class="social-item">
        <a href="<?php echo get_informacao('instagram')?>" class="social-link" target="_blank">
            <img src="<?php echo get_template_directory_uri();?>/assets/img/instagram.svg" class="svg social-svg">
        </a>
    </li>
    <?php }?>
    <?php if((get_informacao('behance'))){?>
    <li class="social-item">
        <a href="<?php echo get_informacao('behance')?>" class="social-link" target="_blank">
            <img src="<?php echo get_template_directory_uri();?>/assets/img/behance.svg" class="svg social-svg">
        </a>
    </li>
    <?php }?>
    <?php if((get_informacao('dribble'))){?>
    <li class="social-item">
        <a href="<?php echo get_informacao('dribble')?>" class="social-link" target="_blank">
            <img src="<?php echo get_template_directory_uri();?>/assets/img/dribble.svg" class="svg social-svg">
        </a>
    </li>
    <?php }?>
    <?php if((get_informacao('pinterest'))){?>
    <li class="social-item">
        <a href="<?php echo get_informacao('pinterest')?>" class="social-link" target="_blank">
            <img src="<?php echo get_template_directory_uri();?>/assets/img/pinterest.svg" class="svg social-svg">
        </a>
    </li>
    <?php }?>
</ul>