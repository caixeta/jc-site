<section id="contato-home-section" class="clearfix">
	<div class="left-content">	
		<div class="row">			
			<?php 
				$current_language = pll_current_language('locale');
	 			$contatoString = 'Contato';

	 			if($current_language == 'en_US'){
	 				$contatoString = 'Contact';
	 			}
	 			if($current_language == 'es_ES'){
	 				$contatoString = 'Contacto';
	 			}
			?>
			<h2 class="col-xs-6 col-sm-3 col-md-2 texto-maiusculo titulo-section row-sm-down"><?php echo $contatoString;?></h2>
			<div class="col-sm-7 col-md-5">
				<p><a href="mailto:<?php echo get_informacao('endemail')?>?subject=Contato Site" class="big-text texto-maiusculo">
					<b><?php echo get_informacao('endemail')?></b>
				</a></p>
				</br>
				<p><b class=" texto-cor-secundaria"><?php echo get_informacao('pais_ddd')?></b> <b class="medium-text"><?php echo get_informacao('telefone')?></b></p>	</br>				
				<?php include('social_list.php');?>
			</div>
		</div>
	</div>		
</section>