<?php 

    $args = array(
            'taxonomy'      => 'portifolios',
            'orderby'       => 'name',
            'order'         => 'ASC',
            'hide_empty'    => true,

            );
    $categorias = get_terms($args);
    $actual_cat_slug = get_queried_object()->slug;
?>
<div class="toggler-wrapper clearfix">
    <button data-toggle="collapse" data-target="#portifolio_categ_collapse" class="btn_categ_toggler">
        <svg version="1.1" id="Categ_collapse" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
         width="475.299px" height="475.299px" viewBox="0 0 475.299 475.299" style="enable-background:new 0 0 475.299 475.299;"
         xml:space="preserve">
        <g>
            <path d="M458.159,86.986H169.971c-9.453,0-17.14,7.688-17.14,17.141v75.12H17.141C7.688,179.247,0,186.935,0,196.385v174.783
                c0,9.458,7.688,17.145,17.141,17.145h288.185c9.458,0,17.14-7.687,17.14-17.145v-75.119h135.694c9.45,0,17.14-7.684,17.14-17.139
                V104.127C475.291,94.674,467.605,86.986,458.159,86.986z M305.656,371.168c0,0.181-0.145,0.334-0.331,0.334l-288.509-0.334
                l0.331-175.112l288.509,0.329V371.168z M458.481,278.91c0,0.181-0.146,0.329-0.332,0.329l-135.685-0.153v-82.701
                c0-9.45-7.694-17.138-17.145-17.138H169.826l0.141-75.451l288.51,0.331V278.91H458.481z"/>
        </g>
        <g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g>
    </svg>
    </button>
</div>
<ul class="nav nav-justified categorias-portfolios-list collapse navbar-collapse" id="portifolio_categ_collapse">
    <?php
        $current_language = pll_current_language('locale');
        $destaqueCat = 'destaques';
        $portfolioCat = 'portifolio';        
        $pagePortifolio = get_id_by_slug( 'portfolios-pt' );
        $potfolioTodos = 'Todos';
        if($current_language == 'en_US'){
            $destaqueCat = 'featured';
            $portfolioCat = 'portfolio';     
            $pagePortifolio = get_id_by_slug( 'portfolios-en' );
            $potfolioTodos = 'All';
        }
        if($current_language == 'es_ES'){
            $destaqueCat = 'destaques-es';
            $portfolioCat = 'cartera';     
            $pagePortifolio = get_id_by_slug( 'portfolios-es' );
            $potfolioTodos = 'Todos';
        }
        $pagePortifolioURL = get_post_permalink($pagePortifolio); 
        ?>
        <li class="categorias-portfolios-item">
            <a href="<?php echo $pagePortifolioURL;?>" class="categorias-portfolios-link "><?php echo $potfolioTodos;?></a>
        </li>
        <?php
        foreach ($categorias as $categoria) {
            if(((strcmp($categoria->slug, $destaqueCat)) && (strcmp($categoria->slug, $portfolioCat)))) {
                $categoriaLink = get_term_link( $categoria->slug, 'portifolios');
                ?>            
        <li class="categorias-portfolios-item">
            <a href="<?php echo $categoriaLink;?>" class="categorias-portfolios-link <?php if(!strcmp($actual_cat_slug,$categoria->slug)){ echo 'categorias-portfolios-link-active';}?>"><?php echo $categoria->name;?></a>
        </li>
            <?php }
        }
    ?>
</ul>