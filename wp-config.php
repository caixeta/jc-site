<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */
 
// Include local configuration
if (file_exists(dirname(__FILE__) . '/local-config.php')) {
	include(dirname(__FILE__) . '/local-config.php');
}

// Global DB config
if (!defined('DB_NAME')) {
	define('DB_NAME', 'wp_jc');
}
if (!defined('DB_USER')) {
	define('DB_USER', 'admin');
}
if (!defined('DB_PASSWORD')) {
	define('DB_PASSWORD', 'admin123');
}
if (!defined('DB_HOST')) {
	define('DB_HOST', 'localhost');
}

/** Database Charset to use in creating database tables. */
if (!defined('DB_CHARSET')) {
	define('DB_CHARSET', 'utf8');
}

/** The Database Collate type. Don't change this if in doubt. */
if (!defined('DB_COLLATE')) {
	define('DB_COLLATE', '');
}

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'xs6u@~w0qkv?H{wjltr183`-a(;GDF5j{V+J+Ktdnxomcu.F!<N<L?[Z+r]LR*V=');
define('SECURE_AUTH_KEY',  '_iVMm:ut{eLmgQ-9r-w,e=|VcQW|]s)F:1@6+<bj2gaWcr!-u${jmSVS_{cz? |i');
define('LOGGED_IN_KEY',    'OhV<;swK3v=U;{%nXyPa[HbC[ hafx|3-j/jf S6b2K|Ox`fY|<XsvF4D-+ZPcIJ');
define('NONCE_KEY',        'gTD$0_ <AcZjAq%?SXV`xV0.2%c5|&ux7@U&F1h]gv>_GXLYMswQ}M28u8%&,)+l');
define('AUTH_SALT',        '9~!tmYMGzz_f9,`gWg6{7;>6zTeO/B5M-J>1]TM|kQc,N+]9H*#mi7P@6:Z@NOq<');
define('SECURE_AUTH_SALT', 'q+`?AR>v@`Vz32>dxLBXqWW-hA+dv;wQh/P%KDC6[N|T_.7J|P!~QOjzr_@[JLT7');
define('LOGGED_IN_SALT',   '+@|$g*E*H??siqXy& U!4#HAB)mmN8U9n#2e*C?APvLZ^:9Y=~|_HQc?B|/vQ-ua');
define('NONCE_SALT',       'fXn^+]9Kroy5<r-2z+WmA(,RH~x(laCaP1rW-v#)+j9Lsk7r`vS/x E=|8RKWj~s');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'jcwp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');


/**
 * Set custom paths
 *
 * These are required because wordpress is installed in a subdirectory.
 */

if (!defined('WP_SITEURL')) {
	define('WP_SITEURL', 'http://' . $_SERVER['SERVER_NAME'] . '/jc_design/core');
}
if (!defined('WP_HOME')) {
	define('WP_HOME',    'http://' . $_SERVER['SERVER_NAME'] . '/jc_design');
}
if (!defined('WP_CONTENT_DIR')) {
	define('WP_CONTENT_DIR', dirname(__FILE__) . '/src');
}
if (!defined('WP_CONTENT_URL')) {
	define('WP_CONTENT_URL', 'http://' . $_SERVER['SERVER_NAME'] . '/jc_design/src');
}


/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
if (!defined('WP_DEBUG')) {
	define('WP_DEBUG', false);
}

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
